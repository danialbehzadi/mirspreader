#! /usr/bin/env python3
# Danial Behzadi - 2018
# Released under AGPLv3

from bs4 import BeautifulSoup as BS
from config import *
from mastodon import Mastodon, StreamListener
from time import sleep
from persian_bad_words.list import bad_words
from persian_bad_words.normalizer import normalize

mastodon = Mastodon(
        access_token = access_token,
        api_base_url = api_base_url
        )

def get_text(toot):
    content = BS(toot['content'],
            features='html.parser')
    text = content.get_text()
    return text

def is_blocked(user):
    id = user['id']
    relationship = mastodon.account_relationships(id)
    stat = relationship[0]['blocking']
    return stat

def rebloged(toot):
    if 'reblogged' in toot:
        return True
    return False

def user_not_retweetable(toot):
    user = toot['account']
    if is_blocked(user):
        return True
    return False

def is_reply(toot):
    if toot['in_reply_to_id']:
        return True
    return False

def has_bad_words(toot):
    text = get_text(toot)
    if any(bad_word in normalize(text) for bad_word in bad_words):
        return True
    return False

def has_filtered_words(toot):
    #filters = mastodon.filters()
    return False#TODO implement

def reblogable(toot):
    if rebloged(toot):
        return False
    if user_not_retweetable(toot):
        return False
    if is_reply(toot):
        return False
    if has_bad_words(toot):
        return False
    if has_filtered_words(toot):
        return False
    return True

def check_for_boost(toot):
    if reblogable(toot):
        id = toot["id"]
        mastodon.status_reblog(id)

class Listener(StreamListener):
    def on_update(self, status):
        check_for_boost(status)

def main():
    # Check for previous toots
    toots = mastodon.timeline_hashtag(query)
    for toot in toots:
        check_for_boost(toot)
    # Go Live
    listener = Listener()
    while True:
        try:
            mastodon.stream_hashtag(query, listener, timeout=30)
        except:
            pass
        sleep(60)

if __name__ == '__main__':
    main()
