#!/usr/bin/python3
# Released under GPLv3+ License
# Danial Behzadi<dani.behzi@ubuntu.com>, 2020-2023.
"""
Mirspreader Project
"""

from time import sleep
from bs4 import BeautifulSoup as BS
from mastodon import Mastodon, StreamListener
from persian_bad_words.list import bad_words
from persian_bad_words.normalizer import normalize
import config


mastodon = Mastodon(
    access_token=config.ACCESS_TOKEN, api_base_url=config.API_BASE_URL
)


def get_text(post):
    """
    get the text of post
    """
    content = BS(post["content"], features="html.parser")
    return content.get_text()


def is_blocked(user):
    """
    check if the poster is blocked
    """
    user_id = user["id"]
    relationship = mastodon.account_relationships(user_id)
    return relationship[0]["blocking"]


def rebloged(post):
    """
    check if the post is a boost of an original post
    """
    return post["reblogged"]


def user_not_reblogable(post):
    """
    check the eligibilty of the poster
    """
    user = post["account"]
    return is_blocked(user)


def is_reply(post):
    """
    check if the post is a reply to another post
    """
    return bool(post["in_reply_to_id"])


def has_bad_words(post):
    """
    check post agianst bad word list
    """
    text = normalize(get_text(post))
    return any(filter(lambda bad_word: bad_word in text, bad_words))


# def has_filtered_words(post): #  TODO: implement
#     filters = mastodon.filters()
#     return False


def reblogable(post):
    """
    check the eligibility of the post
    """
    if rebloged(post):
        return False
    if user_not_reblogable(post):
        return False
    if is_reply(post):
        return False
    if has_bad_words(post):
        return False
    # if has_filtered_words(post):
    #     return False
    return True


def check_for_boost(post):
    """
    Boost the post if it's eligible
    """
    if reblogable(post):
        post_id = post["id"]
        mastodon.status_reblog(post_id)


class Listener(StreamListener):
    """
    This is the main Mastodon Lstream listener
    """

    def on_update(self, status):
        check_for_boost(status)


def main():
    """
    First check for previous post, the go live
    """
    # Check for previous posts
    posts = mastodon.timeline_hashtag(config.QUERY)
    for post in posts:
        check_for_boost(post)
    # Go Live
    listener = Listener()
    while True:
        try:
            mastodon.stream_hashtag(config.QUERY, listener, timeout=30)
        except Exception as inst:
            print(inst)
        sleep(60)


if __name__ == "__main__":
    main()
