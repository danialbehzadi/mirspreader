#!/bin/bash
# Released under GPLv3+ License
# Danial Behzadi<dani.behzi@ubuntu.com>, 2020-2021

export SHELL=/bin/bash
cd "$( dirname "${BASH_SOURCE[0]}" )"
source .env/bin/activate
python3 mirspreader.py
