This is a python3 port of Mirspreader project which was originally written in php5. You can find a running instance of this bot [here](https://botsin.space/@mirspreader).

It was a Twitter bot at first, but since Twitter sucks, I ported it to Mastodon. You can still find the Twitter version in [it's branch](https://framagit.org/danialbehzadi/mirspreader/tree/twitter).

Install
=======
To make it work, do the following:

    $ git clone --recurse-submodules https://framagit.org/danialbehzadi/mirspreader.git
    $ cd mirspreader
    $ python3 -m venv .env
    $ source .env/bin/activate
    (.env)$ pip3 install -r requirements.txt

Config
======
Make a copy of config file:

    $ cp config.py.template config.py

Fill out `config.py` file with your instance's base url and the access token you got from it. I recommend the [Bots in Space](https://botsin.space) instance, since it's dedicated to bots.

Run
===
To run the bot do as following:

    $ ./run.sh

If it runs correctly, you can set a cron job to make it run at boot.
